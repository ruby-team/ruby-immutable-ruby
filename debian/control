Source: ruby-immutable-ruby
Section: ruby
Priority: optional
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Cédric Boutillier <boutil@debian.org>
Build-Depends: debhelper-compat (= 13),
               gem2deb (>= 1),
               ruby-concurrent (>= 1.1),
               ruby-rspec,
               ruby-sorted-set (>= 1.0)
Standards-Version: 4.6.1
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-immutable-ruby.git
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-immutable-ruby
Homepage: https://github.com/immutable-ruby/immutable-ruby
Testsuite: autopkgtest-pkg-ruby
Rules-Requires-Root: no

Package: ruby-immutable-ruby
Architecture: all
Depends: ${misc:Depends},
         ${ruby:Depends},
         ${shlibs:Depends}
Description: efficient, immutable, thread-safe collection classes for Ruby
 The immutable-ruby gem provides 6 Persistent Data Structures: Hash, Vector,
 Set, SortedSet, List, and Deque (which works as an immutable queue or stack).
 .
 Whenever you "modify" an Immutable collection, the original is preserved and a
 modified copy is returned. This makes them inherently thread-safe and
 shareable. At the same time, they remain CPU and memory-efficient by sharing
 between copies.
 .
 Immutable was forked from Simon Harris' Hamster library (ruby-hamster
 package), which is no longer maintained. It features some bug fixes and
 performance optimizations which are not included in Hamster. Aside from the
 name of the top-level module, the public API is virtually identical.
